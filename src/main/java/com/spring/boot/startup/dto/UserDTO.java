/**
 * 
 */
package com.spring.boot.startup.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.spring.boot.startup.domain.User;

/**
 * @author YashPal Singh
 *
 */
public class UserDTO implements Serializable {

	private static final long serialVersionUID = -1890483026110842605L;

	private String message = "No record found.";
	private List<User> users = new ArrayList<User>();

	public String getMessage() {
		if (!users.isEmpty())
			message = "Success";
		return message;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}
