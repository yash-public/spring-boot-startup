package com.spring.boot.startup.config;

import java.net.URI;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataBaseConfig {

	@Value("${spring.datasource.driver-class-name}")
	private String driverName;
	
	@Bean
	public DataSource dataSource() {
		HikariConfig config = new HikariConfig();
		String connectionString = System.getenv().get("DATABASE_URL");
		System.out.println("====================Database URL parsed:" + connectionString);
		try {
			URI dbUri = new URI(connectionString);
			String dbPassword = extractDBPasswordFromDBUri(dbUri);
			String dbUsername = extractDBUserNameFromDBUri(dbUri);
			String dbUrl = createConnectionStringInJDBCFormatFromDBUriForPostgres(dbUri);

			DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create(this.getClass().getClassLoader());
			dataSourceBuilder.driverClassName(driverName).url(dbUrl).username(dbUsername).password(dbPassword);
			dataSourceBuilder.type(HikariDataSource.class);
			config.setDataSource(dataSourceBuilder.build());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		HikariDataSource dataSource = new HikariDataSource(config);
		return dataSource;
	}

	private String extractDBPasswordFromDBUri(URI dbUri) {
		String password = dbUri.getUserInfo().split(":")[1];
		System.out.println("====================Database password parsed: " + password);
		return password;
	}

	private String extractDBUserNameFromDBUri(URI dbUri) {
		String userName = dbUri.getUserInfo().split(":")[0];
		System.out.println("====================Database username parsed: " + userName);
		return userName;
	}

	private String createConnectionStringInJDBCFormatFromDBUriForPostgres(URI dbUri) {
		return "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath()
				+ "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
	}

}
