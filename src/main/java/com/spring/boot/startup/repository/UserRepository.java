/**
 * 
 */
package com.spring.boot.startup.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.spring.boot.startup.domain.User;

/**
 * @author YashPal Singh
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

}
