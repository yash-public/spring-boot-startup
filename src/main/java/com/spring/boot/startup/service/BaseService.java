/**
 * 
 */
package com.spring.boot.startup.service;

import java.util.Optional;

/**
 * @author YashPal Singh
 *
 */
public interface BaseService<T> {

	Optional<T> findById(Integer id);
	Iterable<T> findAll();
	
}
