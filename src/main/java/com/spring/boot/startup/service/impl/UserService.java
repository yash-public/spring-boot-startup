/**
 * 
 */
package com.spring.boot.startup.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.boot.startup.domain.User;
import com.spring.boot.startup.dto.UserDTO;
import com.spring.boot.startup.repository.UserRepository;
import com.spring.boot.startup.service.BaseService;

/**
 * @author YashPal Singh
 *
 */
@Service
public class UserService implements BaseService<User> {

	@Autowired
	private UserRepository repository;

	@Override
	public Optional<User> findById(Integer id) {
		return repository.findById(id);
	}

	@Override
	public Iterable<User> findAll() {
		return repository.findAll();
	}

	public UserDTO findAllUsers() {
		UserDTO dto = new UserDTO();
		repository.findAll().forEach(user -> dto.getUsers().add(user));
		return dto;
	}

	public User addUser(User user) {
		return repository.save(user);
	}

}
