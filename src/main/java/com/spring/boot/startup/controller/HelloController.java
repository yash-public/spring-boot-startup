/**
 * 
 */
package com.spring.boot.startup.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author YashPal Singh
 *
 */
@RestController
public class HelloController {

	@RequestMapping(value = "/hello", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> printHello() {
		return new ResponseEntity<String>("Hello Yashpal!", HttpStatus.OK);
	}

}
