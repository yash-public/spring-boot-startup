/**
 * 
 */
package com.spring.boot.startup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.boot.startup.domain.User;
import com.spring.boot.startup.dto.UserDTO;
import com.spring.boot.startup.service.impl.UserService;

/**
 * @author YashPal Singh
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService service;

	@RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> findAllUsers() {
		try {
			return new ResponseEntity<UserDTO>(service.findAllUsers(), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<UserDTO>(new UserDTO(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> addUser(@RequestBody User user) {
		try {
			return new ResponseEntity<User>(service.addUser(user), HttpStatus.CREATED);
		} catch (Exception ex) {
			return new ResponseEntity<User>(new User(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
